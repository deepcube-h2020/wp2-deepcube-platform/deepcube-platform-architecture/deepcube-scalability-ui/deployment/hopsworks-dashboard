CREATE DATABASE IF NOT EXISTS node_gestion;

USE node_gestion;

CREATE TABLE IF NOT EXISTS node (
  id_node INT(11) PRIMARY KEY AUTO_INCREMENT,
  uuid VARCHAR(255) COLLATE utf8_general_ci NOT NULL,
  email VARCHAR(255) COLLATE utf8_general_ci NOT NULL,
  creation_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  activate_date TIMESTAMP NULL,
  destroy_date TIMESTAMP NULL,
  usecase VARCHAR(255) COLLATE utf8_general_ci NOT NULL,
  type VARCHAR(100) COLLATE utf8_general_ci NOT NULL,
  duration INT(11) NOT NULL,
  status VARCHAR(100) COLLATE utf8_general_ci NOT NULL,
  hostname VARCHAR(200) COLLATE utf8_general_ci NULL,
  ip VARCHAR(100) COLLATE utf8_general_ci NULL
);
