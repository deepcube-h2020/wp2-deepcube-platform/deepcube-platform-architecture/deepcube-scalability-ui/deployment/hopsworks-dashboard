# Hopsworks Dashboard



# API HOPSWORKS




### GET ENDPOINT

#### Prefix

```http
  GET /api/v1
```

#### Get all nodes

```http
  GET /api/v1/node/list
```

#### Get pending provisioning nodes

```http
  GET /api/v1/node/list/pendingprovisioning
```

#### Get pending destroy nodes

```http
  GET /api/v1/node/list/pendingdestroy
```

#### Get all types

```http
  GET /api/v1/type/list
```

#### Get all usecase

```http
  GET /api/v1/usecase/list
```

### POST ENDPOINT

#### POST changes node pending provisioning to provisioning

```http
  POST /api/v1/node/provisioning
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `uuid`      | `string` | **Required**. uuid of item to fetch |

#### POST changes node provisioning to activate

```http
  POST /api/v1/node/activate
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `uuid`      | `string` | **Required**. uuid of item to fetch |

#### POST changes node active to pending destroy

```http
  POST /api/v1/node/pendingdestroy
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `uuid`      | `string` | **Required**. uuid of item to fetch |

#### DELETE changes node pending destroy to destroy

```http
  DELETE /api/v1/node/destroy
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `uuid`      | `string` | **Required**. uuid of item to fetch |

## Usage/Examples with curl

```bash
curl -X DELETE -H "Content-Type: application/x-www-form-urlencoded" -d 'uuid=${uuid}' {protocole}://{host}/api/v1/node/provisioning
```


## Authors

- [Driss Benadjal]()


## Running Tests

To run tests, run the following command

```bash
  npm run test
```

